require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = FileList['./spec/**/*_spec.rb'] - FileList['./spec/unit/workstation_config_loader_spec.rb']
end
